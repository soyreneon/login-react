# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Se trata de un proyecto que consiste en un login y el manejo del estado de la aplicación

## Tecnologias utilizadas

react-hooks
redux
react-router
tailwind-css
postcss
headless-ui
hero-icons
