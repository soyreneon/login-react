import { Fragment } from 'react'
import { Menu, Transition } from '@headlessui/react'
import { UserCircleIcon } from '@heroicons/react/solid'
import { NavLink } from 'react-router-dom'
import useAuth from '../auth/useAuth'

const ProfileDropdown = () => {
    const auth = useAuth()
    return (
        <Menu as="div" className="ml-3 relative">
            {({ open }) => (
                <>
                    <div>
                        <Menu.Button className="bg-gray-800 flex text-sm rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white">
                            <span className="sr-only">Abrir menu de usuario</span>
                            {(!auth.isLogged() || (auth.getImg().trim()==='')  ) ? (
                                <UserCircleIcon className="h-8 w-8 bg-gray-800 rounded-full text-gray-400 hover:text-white focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white" aria-hidden="true" />
                            ) : (
                                <img
                                    className="h-8 w-8 rounded-full"
                                    src={auth.getImg()}
                                    alt=""
                                />
                            )}
                        </Menu.Button>
                    </div>
                    <Transition
                        show={open}
                        as={Fragment}
                        enter="transition ease-out duration-100"
                        enterFrom="transform opacity-0 scale-95"
                        enterTo="transform opacity-100 scale-100"
                        leave="transition ease-in duration-75"
                        leaveFrom="transform opacity-100 scale-100"
                        leaveTo="transform opacity-0 scale-95"
                    >
                        {!auth.isLogged() ? (
                            <Menu.Items
                                static
                                className="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none"
                            >
                                <Menu.Item>
                                    {({ active }) => (
                                        <NavLink
                                            to="/login"
                                            activeClassName="activeprofiledropdown"
                                            exact
                                            className={`${active ? 'bg-gray-100' : ''
                                                } block px-4 py-2 text-sm text-gray-700`}
                                        >
                                            Log in
                                        </NavLink>
                                    )}
                                </Menu.Item>
                            </Menu.Items>
                        ) : (
                            <Menu.Items
                                static
                                className="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none"
                            >
                                <Menu.Item>
                                    {({ active }) => (
                                        <NavLink
                                            to="/preferencias"
                                            activeClassName="activeprofiledropdown"
                                            className={`${active ? 'bg-gray-100' : ''
                                                } block px-4 py-2 text-sm text-gray-700`}
                                        >
                                            Preferencias
                                        </NavLink>
                                    )}
                                </Menu.Item>
                                <Menu.Item>
                                    {({ active }) => (
                                        <NavLink
                                            to="/dashboard"
                                            activeClassName="activeprofiledropdown"
                                            className={`${active ? 'bg-gray-100' : ''
                                                } block px-4 py-2 text-sm text-gray-700`}
                                        >
                                            Dashboard
                                        </NavLink>
                                    )}
                                </Menu.Item>
                                <Menu.Item>
                                    {({ active }) => (
                                        <button
                                            onClick={auth.logout}
                                            className={`${active ? 'bg-gray-100' : ''
                                                } inline-flex w-full px-4 py-2 text-sm text-gray-700`}
                                        >
                                            Sign out
                                        </button>
                                    )}
                                </Menu.Item>
                            </Menu.Items>
                        )}

                    </Transition>
                </>
            )}
        </Menu>
    );
}

export default ProfileDropdown;