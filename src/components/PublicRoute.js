import { Route, Redirect } from "react-router-dom";
import useAuth from "../auth/useAuth";

// este componente evita poder entrar a lugares donde no deberiamos cuando ya estamos logueaos
// por ejemplo el login
export default function PublicRoute({ component: Component, ...rest }) {
  const auth = useAuth();
//console.log(auth.isLogged())
  return (
    <Route {...rest}>
      {!auth.isLogged() ? (
        <Component />
      ) : (
        <Redirect to="/dashboard" />
      )}
    </Route>
  );
}