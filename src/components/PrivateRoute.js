import { Route, Redirect, useLocation } from "react-router-dom";
import useAuth from "../auth/useAuth";

// rcibimos el componente por separado y el resto de atributos mediante reat params
export default function PrivateRoute({ component: Component, ...rest }) {
  
  const auth = useAuth();
  const location = useLocation();

  return (
    <Route {...rest}>
      { auth.isLogged()  ? ( 
        <Component />
      ) : (
        <Redirect to={{ pathname: "/login" , state: { from: location }}} />
      )}
    </Route>
  );
}
