import { createContext, useState, useEffect } from "react";

/// contexto a consumir
export const AuthContext = createContext();


// provedor del contexto (envuelve a la app en un nivel alto para alcanzar la app)
const AuthProvider = ({ children }) => {
  const [user, setUser] = useState(
    JSON.parse(localStorage.getItem("user")) || null
  );
  //console.log(JSON.parse(localStorage.getItem("user")))

  useEffect(() => {
    try {
      console.log('disparado el efecto')
      localStorage.setItem("user", JSON.stringify(user));
    } catch (error) {
      localStorage.removeItem("user");
      console.log(error);
    }
  }, [user]);

  // aqui almacenamos todo el storage de las cosas que queremos
  const contextValue = {
    user,
    login(name, password) { // aqui deberiamos hacer una peticion al api rest para validar
      setUser({
        id: 1,
        username: name,
        imgprofile: 'https://picsum.photos/id/1062/100'
      }); // APIrest nos devolveria algo asi 
    },
    logout() {
      setUser(null);
    },
    getImg() {
      if (user) // si no es null
        return user.imgprofile
      return ''
    },
    isLogged() {
      return !!user; // si es nulo, !! retorna falso
    },
  };

  // pasamos al provider el objeto para el contexto
  return (
    <AuthContext.Provider value={contextValue}>{children}</AuthContext.Provider>
  );
};

export default AuthProvider;
