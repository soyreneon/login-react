import { useContext } from 'react';
import { AuthContext } from './AuthProvider';

// aqui consumimos en contexto con el hook useContext
export default function useAuth() {
    return  useContext(AuthContext);
}
