import { useForm } from "react-hook-form"
import useAuth from "../../auth/useAuth"
import { useHistory, useLocation } from 'react-router-dom'
/* import PropTypes from 'prop-types' */

// llamada a la api en espera de token
//async function loginUser(credentials) {
  /*  return fetch('https://jsonplaceholder.typicode.com/users/1', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(credentials)
  })
    .then( data =>  data.json() )
  */
  // retornamos valor default
  //return {token: '102030'}
 //}

const Login = () => {
  const history = useHistory();
  const location = useLocation();
  const previusObjectURL = location.state?.from;
  const auth = useAuth();

  const { register, handleSubmit, formState: { errors } } = useForm();
  const onSubmit = async data => {
/*    //console.log(data)
    const token = await loginUser({
      user: data.user,
      password: data.password
    });
     // aqui deberiamos comprobar si nos regresa un token o algun código de error
    setToken(token);
    //console.log(token) */
    auth.login(data.user, data.password);
    history.push(previusObjectURL || "/dashboard")
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)} className="grid grid-cols-8 gap-4">
      <div className="col-start-4 col-span-2 bg-gray-100 mt-5 p-5 shadow-md">
        <label className="block">
          <p className="mb-1">User</p>
          <input type="text" placeholder="User" className="default"
            {...register("user",
              {
                required: true,
                maxLength: 50
              })} />

          {errors.user && errors.user.type === "maxLength" && (
            <span className="block mb-3 text-red-700">Maxima longitud excedida</span>
          )}
          {errors.user && errors.user.type === "required" && (
            <span className="block my-2 text-red-700">El usuario es requerido</span>
          )}
        </label>
        <label className="mt-3 block">
          <p className="mb-1">Password</p>
          <input type="password" placeholder="" className="default" {...register("password",
            {
              required: true
            })} />

          {errors.password && errors.password.type === "required" && (
            <span className="block my-2 text-red-700">El password es requerido</span>
          )}
        </label>
        <div className="mt-10 block">
          <button type="submit" className="w-full rounded py-1.5 text-xl border-2 border-blue-700 text-white bg-blue-600 active:bg-blue-900 hover:bg-blue-500 ">Submit</button>
        </div>
      </div>
    </form>
  );
}

export default Login;

/* // aseguramos que la propiedad recibida sea una funcion
Login.propTypes = {
  setToken: PropTypes.func.isRequired
}
 */