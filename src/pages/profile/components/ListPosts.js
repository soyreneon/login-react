import useFetch from '../../../hooks/useFetch'
import Loading from '../../../components/Loading'
import FatalError from '../../../pages/500'
import { Link, useLocation } from 'react-router-dom'
import { PencilAltIcon } from '@heroicons/react/outline'
import Pagination from './Pagination'

const ListPost = () => {
    const location = useLocation()
    const query = new URLSearchParams(location.search)
    // obtenemos los parametros de la url
    const skip = parseInt(query.get('page')) || 1
    const limit = parseInt(query.get('limit')) || 10
    //console.log(skip)

    const { data, error, loading } = useFetch(`https://jsonplaceholder.typicode.com/posts/?_page=${skip}&_limit=${limit}`)
    //'https://jsonplaceholder.typicode.com/posts/'
    // https://jsonplaceholder.typicode.com/posts/?_start=1&_limit=4
    // https://jsonplaceholder.typicode.com/posts/?_page=1&_limit=4
    // https://jsonplaceholder.typicode.com/users/1/posts
    return (<div>
        {
            loading &&
            <Loading />
        }
        {
            error &&
            <FatalError msg="Hubo un error al realizar la solicitud" />
        }
        {
            data && (
                data.map(item => {
                    return (
                        <div key={item.id} className="flex flex-1 mt-2 bg-gray-300">

                            <div className="w-7 bg-blue-700 text-center">
                                <h1 className="h-10 inline-block align-middle">
                                    {item.id}
                                </h1>
                            </div>

                            <div className="flex-grow w-1/2 bg-blue-300">
                                <p className="card-text">{`${item.title}   `}</p>
                            </div>
                            <div className="w-12 bg-red-300 text-center p-1">
                                <Link to={
                                    {
                                        pathname: `dashboard/item/${item.id} `,
                                    }
                                } className="inline-block text-center">
                                    <PencilAltIcon className="h-6 w-6 text-gray-400 hover:text-white focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white" aria-hidden="true" />
                                </Link>
                            </div>
                        </div>
                    )
                })
            )
        }
        <Pagination
            skip={skip}
            limit={limit}
            query={query} />
    </div>);
}

export default ListPost;