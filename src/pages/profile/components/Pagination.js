import { NavLink, useHistory } from 'react-router-dom'
import { ChevronLeftIcon, ChevronRightIcon } from '@heroicons/react/outline'

const Pagination = ({ skip, limit, query }) => {
    const history = useHistory()
    const handleBeforePagination = (action) => {
        if ((action === '+') && (skip < 10)) {
            // colocamos los parametros en el objeto
            query.set("page", ++skip)
        } else if ((action === '-') && (skip > 1)) {
            query.set("page", --skip)
        }
        // cambiamos el valor de la url(solo el query) del browser cololando el objeto query
        history.push({ search: query.toString() })
    }

    const PathNavLink = (props) => (
        <NavLink isActive={
            (_, { pathname }) => {
                return pathname+'?page='+query.get("page") === '/'+props.to
            }
        }
            {...props} />
    );

    const createPageNumbers = () => {
        let numbers = []
        let classes = "border-2 border-indigo-300  mx-2 p-1 w-8 rounded-full h-9 w-9 flex items-center justify-center hover:border-indigo-500 hover:text-indigo-600"
        numbers.push(<button key="back" onClick={() => { handleBeforePagination('-') }} className={`${classes}`} >
            <ChevronLeftIcon className="h-4 w-4" aria-hidden="true" />
        </button>)
        for (let i = 1; i <= 10; i++) {
            numbers.push(
                <PathNavLink key={i} exact to={`dashboard?page=${i}`} className={`${classes}`} activeClassName="bg-indigo-300">
                    {i}
                </PathNavLink>)
        }
        numbers.push(<button key="forward" onClick={() => { handleBeforePagination('+') }} className={`${classes}`}>
            <ChevronRightIcon className="h-4 w-4" aria-hidden="true" />
        </button>)
        return numbers
    }

    return (
        <div className="text-center mt-6 mb-1 flex justify-center items-stretch">
            {createPageNumbers()}
        </div>
    );
}

export default Pagination;