import useFetch from '../../hooks/useFetch'
import Loading from '../../components/Loading'
import FatalError from '../500'
import { useParams, useHistory, Link } from 'react-router-dom'
import { ArrowCircleLeftIcon } from '@heroicons/react/outline'
import { useForm } from "react-hook-form"

const EditItem = () => {
    const { id } = useParams()
    const history = useHistory();
    const { data, error, loading } = useFetch(`https://jsonplaceholder.typicode.com/posts/${id}`)

    const { register, handleSubmit, formState: { errors } } = useForm();
    const onSubmit = async data => {
        console.log(data)
        /*       const token = await loginUser({
                user: data.user,
                password: data.password
              });
         */      // aqui deberiamos comprobar si nos regresa un token o algun código de error
    }


    return (<div className="container mx-auto my-6">
        <div className="flex flex-wrap mb-5 gap-4">
            <button onClick={() => history.goBack()} className="hover:text-white text-gray-200 text-center bg-blue-900 py-2 px-4 rounded">
                <ArrowCircleLeftIcon className="inline-block mr-2 h-6 w-6" aria-hidden="true" />
                <span>Regresar</span>
            </button>
            <Link to="#" className="hover:text-white text-gray-200 text-center bg-green-600 py-2 px-4 rounded">
                <ArrowCircleLeftIcon className="inline-block mr-2 h-6 w-6" aria-hidden="true" />
                <span>Guardar y regresar</span>
            </Link>
        </div>
        {
            error &&
            <FatalError msg="Hubo un error al realizar la solicitud" />
        }
        {
            loading &&
            <Loading />
        }
        {
            data && (
                <form onSubmit={handleSubmit(onSubmit)} className="grid grid-cols-5 gap-4">
                    <label className="col-start-2 col-span-3">
                        <p className="mb-1">Titulo</p>
                        <input type="text" placeholder="Titulo" defaultValue={data.title} className="default"
                            {...register("titulo",
                                {
                                    required: true,
                                    maxLength: 100
                                })} />

                        {errors.titulo && errors.titulo.type === "maxLength" && (
                            <span className="block mb-3 text-red-700">Maxima longitud excedida</span>
                        )}
                        {errors.titulo && errors.titulo.type === "required" && (
                            <span className="block my-2 text-red-700">El titulo es requerido</span>
                        )}
                    </label>
                    <label className="col-start-2 col-span-3">
                        <p className="mb-1">Cuerpo</p>
                        <textarea type="text" placeholder="Cuerpo" defaultValue={data.body} className="default h-40"
                            {...register("Cuerpo",
                                {
                                    maxLength: 300
                                })} />

                        {errors.Cuerpo && errors.Cuerpo.type === "maxLength" && (
                            <span className="block mb-3 text-red-700">Maxima longitud excedida</span>
                        )}
                    </label>
                </form>
            )
        } </div>)
}

export default EditItem;