import ListPost from './components/ListPosts'

const Dashboard = () => {
    return (
        <div className="container mx-auto">
            <div className="grid grid-cols-1 gap-5">
                <div className="">
                    <h1 className="text-center my-7 text-xl">Dashboard</h1>
                </div>
            </div>
            <div className="mb-4">
                <ListPost />
            </div>
        </div>);
}

export default Dashboard;