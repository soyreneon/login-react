const Index = () => {
    return (
        <div>
            <div className="min-h-screen flex flex-col">
                <div className="flex h-32">
                    <div className="w-full bg-green-400">1</div>
                </div>
                <div className="flex flex-1">
                    <div className="w-1/4 bg-red-700">2</div>
                    <div className="flex-1">
                        <div className="flex flex-col h-full bg-blue-900">
                            <div className="h-32 bg-blue-400">3</div>
                            <div className="flex h-32 bg-blue-100">
                            <div className="w-1/2 bg-blue-300">4</div>
                            <div className="w-1/2 bg-blue-700">5</div>
                            </div>
                            <div className="flex-1 bg-blue-400">6</div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="flex mt-4 space-x-4 flex-1">
                <div className="w-1/3 bg-gray-700">
                    hola a todos
                </div>
                <div className="flex-auto bg-blue-400">gols</div>
            </div>
        </div>
     );
}

export default Index;