import React from 'react';
import './index.css';
import Navigation from './components/Navigation'
import PrivateRoute from './components/PrivateRoute'
import PublicRoute from './components/PublicRoute'
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";

import Index from './pages/'
import Dashboard from './pages/profile/Dashboard'
import Preferencias from './pages/profile/Preferencias'
import EditItem from './pages/profile/EditItem'
import Login from './pages/login/Login'
import NotFound from './pages/NotFound'
import AuthProvider from "./auth/AuthProvider";

const App = () => {
    return (
        <AuthProvider>
            <Router>
                <Navigation />
                <Switch>
                    <PrivateRoute path="/preferencias" component={Preferencias} />
                    <PrivateRoute path="/dashboard/item/:id" component={EditItem} />
                    <PrivateRoute path="/dashboard" component={Dashboard} />
                    <PublicRoute exact path="/login" component={Login} />
                    <Route path="/mas">
                        Esto solo es una prueba del menú
                    </Route>
                    <Route path="/" exact>
                        <Index />
                    </Route>
                    <Route path="/404" component={NotFound} />
                    <Route path="*">
                        <Redirect to="/404" />
                    </Route>
                </Switch>
            </Router>
        </AuthProvider>
    );
}

export default App;