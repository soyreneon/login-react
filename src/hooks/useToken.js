import { useState } from 'react';
// para almacenar el estado en el local storage dbemos reemplazar sessionStorage por localStorage

const useToken = () => {
  // funcion que obtiene el token
  const getToken = () => {
    const tokenString = sessionStorage.getItem('token');
    const userToken = JSON.parse(tokenString);
    // usamos el ?. encadenamiento opcional por si token no existe(la primera vez)
    return userToken?.token
  };

  // hook para setear token, el primer estado es vacio
  const [token, setToken] = useState(getToken());

  // colocar el token cuando cambia
  const saveToken = userToken => {
    sessionStorage.setItem('token', JSON.stringify(userToken));
    setToken(userToken.token);
  };

  // retorna el valor cambiado
  return {
    setToken: saveToken,
    token
  }
}
export default useToken