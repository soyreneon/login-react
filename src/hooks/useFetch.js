import {useState, useEffect} from 'react'

const useFetch = url => {
    const [data, setData] = useState([])
    const [error, setError] = useState(null)
    const [loading, setLoading] = useState(true)

    // effect se ejecuta despue de cada render o update
    // una forma de tener un ciclo de vida en un componente funcional
    useEffect( () => {

        const fetchResource = async () => {
            try {
                let res = await fetch(url);
                let data = await res.json();

                setLoading(false)
                setData(data)
                //console.log(data)                        
            } catch (error) {
                setLoading(false)
                setError(error)
            }
        }
        fetchResource()

    }, [url])

    return { data, error, loading }
}

export default useFetch